import 'package:permission_handler/permission_handler.dart';

Future<bool> checkPermissionForCameraAndGallery()async{
  Map<Permission,PermissionStatus> status=await[Permission.camera,Permission.storage,].request();
  if(status[Permission.camera].isGranted&&status[Permission.storage].isGranted){
    return true;
  }else{
    return false;
  }
}