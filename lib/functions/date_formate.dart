import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';

String dateFormat(BuildContext context,{String dateTime, bool withTime = false}) {
  DateTime date = DateTime.parse(dateTime);
  String formattedDate = DateFormat("yyyy-MM-dd", "en").format(date);
  String formattedDateWithTime =
      DateFormat("yyyy-MM-dd  h:mm", "en").format(date) +
          DateFormat("a", "en").format(date);
  if (withTime) {
    return formattedDateWithTime;
  } else {
    return formattedDate;
  }
}

String timeFormatter(double time) {
  Duration duration = Duration(milliseconds: time.round());
  return [duration.inHours, duration.inMinutes, duration.inSeconds]
      .map((seg) => seg.remainder(60).toString().padLeft(2, '0'))
      .join(':');
}
