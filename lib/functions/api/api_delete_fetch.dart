import 'dart:async';
import 'dart:convert';
import 'dart:io';

import 'package:game_app/constant/api_state_code.dart';
import 'package:http/http.dart';
import 'package:http/http.dart' as http;

import 'api_state.dart';

Future<Object> apiDeleteFetch(
    {String api_url,
    var model_to_json,
    Map<String, dynamic> body,
    bool reconnect = false,int timer=2000}) async {
  getResponse() async {
    try {
      Map<String, String> header = {
        'Content-Type': 'application/json',
      };
      String jsonBody = json.encode(body);
      Response response;
      response =
          await http.put(Uri.parse(api_url), headers: header, body: jsonBody);
      if (SUCCESS == response.statusCode) {
        return Success(code: SUCCESS, response: model_to_json(response.body));
      }
      if (reconnect) {
        Future.delayed( Duration(milliseconds: timer), () {
          getResponse();
        });
      } else {
        return Failure(
            code: USER_INVALID_RESPONSE, errorResponse: 'Invalid Response');
      }
    } on HttpException {
      if (reconnect) {
        Future.delayed( Duration(milliseconds: timer), () {
          getResponse();
        });
      } else {
        return Failure(
            code: NO_INTERNET_CONNECTION,
            errorResponse: 'No Internet Connection');
      }
    } on SocketException {
      if (reconnect) {
        Future.delayed( Duration(milliseconds:timer), () {
          getResponse();
        });
      } else {
        return Failure(
            code: NO_INTERNET_CONNECTION,
            errorResponse: 'No Internet Connection');
      }
    } on FormatException {
      if (reconnect) {
        Future.delayed( Duration(milliseconds: timer), () {
          getResponse();
        });
      } else {
        return Failure(code: INVALID_FORMAT, errorResponse: 'Invalid Format');
      }
    } catch (e) {
      if (reconnect) {
        Future.delayed( Duration(milliseconds: timer), () {
          getResponse();
        });
      } else {
        return Failure(code: UNKNOWN_ERROR, errorResponse: 'Unknown Error');
      }
    }
  }

 return getResponse();
}
