import 'package:game_app/constant/data.dart';
import 'package:game_app/models/user_model/user_model.dart';

bool userExist(UserModel user) {
  var ex = DataOfApp.userList.where((element) =>
      element.username.toLowerCase().trim() ==
      user.username.toLowerCase().trim());
  if (ex.isEmpty) {
    return false;
  } else {
    return true;
  }
}
