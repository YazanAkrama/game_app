import 'package:json_annotation/json_annotation.dart';

part 'game_model.g.dart';

@JsonSerializable(explicitToJson: true)
class GameModel {
  String id, image, date, created_at, user_id;
  Map<String, String> title, description,geo_location;
  int player_count;

  GameModel(
    this.id,
    this.title,
    this.player_count,
    this.date,
    this.user_id, {
    this.created_at,
    this.description,
    this.geo_location,
    this.image,
  });

  factory GameModel.fromJson(Map<String, dynamic> map) =>
      _$GameModelFromJson(map);

  Map<String, dynamic> toJson() => _$GameModelToJson(this);
}
