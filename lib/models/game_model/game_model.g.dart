// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'game_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GameModel _$GameModelFromJson(Map<String, dynamic> json) {
  return GameModel(
    json['id'] as String,
    (json['title'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    json['player_count'] as int,
    json['date'] as String,
    json['user_id'] as String,
    created_at: json['created_at'] as String,
    description: (json['description'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    geo_location: (json['geo_location'] as Map<String, dynamic>)?.map(
      (k, e) => MapEntry(k, e as String),
    ),
    image: json['image'] as String,
  );
}

Map<String, dynamic> _$GameModelToJson(GameModel instance) => <String, dynamic>{
      'id': instance.id,
      'geo_location': instance.geo_location,
      'image': instance.image,
      'date': instance.date,
      'created_at': instance.created_at,
      'user_id': instance.user_id,
      'title': instance.title,
      'description': instance.description,
      'geo_location': instance.geo_location,
      'player_count': instance.player_count,
    };
