import 'package:flutter/cupertino.dart';

void navigatorToOff(BuildContext context, {String page_name}) async {
  Navigator.of(context).pushNamedAndRemoveUntil(page_name, (Route<dynamic> route) => false);
}
