import 'package:flutter/cupertino.dart';

void navigatorBack(BuildContext context) async {
  Navigator.pop(context);
}
