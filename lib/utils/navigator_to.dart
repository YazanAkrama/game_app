import 'package:flutter/cupertino.dart';

void navigatorTo(BuildContext context, {String page_name,var arguments}) async {
  Navigator.pushNamed(context, page_name,arguments: arguments);
}
