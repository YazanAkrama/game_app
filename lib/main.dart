import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/save_data.dart';
import 'package:game_app/pages/add_page/viewModel/add_controller.dart';
import 'package:game_app/pages/auth/viewModel/auth_controller.dart';
import 'package:game_app/pages/home_page/viewModel/home_page_controller.dart';
import 'package:provider/provider.dart';

import 'constant/app_colors.dart';
import 'constant/route_name.dart';
import 'translations/codegen_loader.g.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await EasyLocalization.ensureInitialized();

  runApp(EasyLocalization(
    path: 'assets/translations',
    supportedLocales: const [
      Locale('en'),
      Locale('ar'),
    ],
    saveLocale: true,
    fallbackLocale: const Locale("en"),
    assetLoader: const CodegenLoader(),
    child: const MyApp(),
  ));
}

class MyApp extends StatelessWidget {
  const MyApp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return MultiProvider(
      providers: [
        ChangeNotifierProvider(
          create: (_) => AuthController(),
        ),
        ChangeNotifierProvider(
          create: (_) => AddController(),
          lazy: true,
        ),
        ChangeNotifierProxyProvider<AddController, HomePageController>(
            create: (BuildContext context) => HomePageController(),
            update: (BuildContext context, AddController addController,
                    HomePageController homeController) =>
                homeController..update()),
      ],
      child: MaterialApp(
        supportedLocales: context.supportedLocales,
        localizationsDelegates: context.localizationDelegates,
        locale: context.locale,
        debugShowCheckedModeBanner: false,
        title: 'game app',
        theme: ThemeData(
          primarySwatch: mainAppColor,
        ),
        initialRoute: "/log_in",
        routes: routes,
        home: Scaffold(
          body: Container(),
        ),
      ),
    );
  }
}

// TODO load translation folder
// to load translation => write this command
// flutter pub run easy_localization:generate -S "assets/translations" -O "lib/translations" -o "locale_keys.g.dart" -f keys

// TODO load json format
// to load json format => write this command
// flutter pub run build_runner build
