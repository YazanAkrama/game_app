import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_string.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/utils/navigator_off_to.dart';
import 'package:game_app/widgets/custom_text.dart';

Widget customDrawer(BuildContext context) {
  return Drawer(
    child: SafeArea(
      child: Column(
        children: [
          context.locale.toString() == "en"
              ? ListTile(
                  title: customText("اللغة العربية",
                      textSize: bigFontSize, textWeight: FontWeight.bold),
                  onTap: () async {
                    await context.setLocale(
                      const Locale('ar'),
                    );
                  },
                )
              : ListTile(
                  title: customText("English",
                      textSize: bigFontSize, textWeight: FontWeight.bold),
                  onTap: () async {
                    await context.setLocale(
                      const Locale('en'),
                    );
                  },
                ),
          ListTile(
            title: customText("log out",
                textSize: bigFontSize, textWeight: FontWeight.bold),
            onTap: () async {
              DataOfApp.myUser = null;
              navigatorToOff(context, page_name: "/log_in");
            },
          ),
        ],
      ),
    ),
  );
}
