import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:game_app/constant/app_string.dart';
import '../constant/app_colors.dart';

Widget customTextField({
  String hintText = "...",
  String initialValue,
  double hintTextHeight = 1,
  IconData prefixIconData,
  double prefixIconSize = 25,
  Color prefixIconColor = mainAppColor,
  IconData suffixIconData,
  double suffixIconSize = 25,
  int maxLines = 1,
  Color suffixIconColor = mainAppColor,
  TextEditingController controller,
  TextInputType inputType = TextInputType.text,
  List<TextInputFormatter> textInputFormatter,
  Alignment alignment = Alignment.center,
  EdgeInsetsGeometry margin,
  EdgeInsetsGeometry padding,
  Color borderColor = mainAppColor,
  double borderRadius = 10,
  double textSize = midFontSize,
  Function(String) onChange,
  Function onTap,
  bool withStar = false,
  bool isDense = true,
}) {
  return Container(
      margin: margin??EdgeInsets.zero,
      padding: padding??EdgeInsets.zero,
      alignment: alignment,
      child: TextFormField(
        initialValue: initialValue,
        controller: controller,
        keyboardType: inputType,
        obscureText: withStar,
        obscuringCharacter: '*',
        onChanged: onChange,
        maxLines: maxLines,
        inputFormatters: textInputFormatter??null,
        onTap: onTap,
        style: TextStyle(fontSize: textSize),
        decoration: InputDecoration(
            isDense: isDense,
            focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.circular(borderRadius),
                borderSide: BorderSide(color: borderColor)),
            border: OutlineInputBorder(
                borderRadius: BorderRadius.circular(borderRadius),
                borderSide: const BorderSide(color: Colors.black)),
            hintText: hintText,
            hintStyle: TextStyle(
              height: hintTextHeight,
            ),
            suffixIcon:suffixIconData!=null? Icon(
              suffixIconData,
              color: suffixIconColor,
              size: suffixIconSize,
            ):null,
            prefixIcon:prefixIconData!=null? Icon(
              prefixIconData,
              color: prefixIconColor,
              size: prefixIconSize,
            ):null),
      ));
}
