
import 'package:flutter/material.dart';

loading(BuildContext context) {
  showDialog(context: context, builder: (context){
    return
      WillPopScope(
        onWillPop: () async {
          return false;
        },
        child: Container(
          color: Colors.black26,
          height: double.maxFinite,
          width: double.maxFinite,
          child: const Center(
            child: CircularProgressIndicator(),
          ),
        ),
      );
  });
}
