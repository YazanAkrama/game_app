import 'package:flutter/material.dart';

import '../constant/app_colors.dart';

Widget customDropDown(
    {double borderRadius = 10,
    Color borderActiveColor = mainAppColor,
    Color borderEnabledColor = Colors.black26,
    Color fillColor = Colors.white,
    Function(dynamic) onChange,
    bool isFilled = true,
    IconData icon,
    var finalValue,
    var value,
    bool sendLocation = false,
    List listItem}) {
  return DropdownButtonFormField(
    decoration: InputDecoration(
        enabledBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(
            Radius.circular(borderRadius),
          ),
          borderSide: BorderSide(
            color: borderEnabledColor,
          ),
        ),
        focusedBorder: OutlineInputBorder(
          borderRadius: BorderRadius.all(Radius.circular(borderRadius)),
          borderSide: BorderSide(
            color: borderActiveColor,
          ),
        ),
        fillColor: fillColor,
        filled: isFilled),
    value: value,
    icon: Icon(icon),
    onChanged: onChange,
    items: listItem.map((valueItem) {
      return DropdownMenuItem(
        value: valueItem,
        child: Text(valueItem),
      );
    }).toList(),
  );
}
