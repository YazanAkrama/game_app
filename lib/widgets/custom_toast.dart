import 'dart:ui';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:game_app/constant/app_colors.dart';


customToast(String msg){
  Fluttertoast.showToast(
    backgroundColor: toast_Color,
    msg: msg,
    toastLength: Toast.LENGTH_SHORT,
    gravity: ToastGravity.SNACKBAR,
  );
}