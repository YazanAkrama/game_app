import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_colors.dart';
import 'package:game_app/constant/app_string.dart';

Widget customText(String text, {
  Color textColor = font_Color_2,
  double textHeight = 1,
  int maxLines,
  TextOverflow overflow = TextOverflow.ellipsis,
  double textSize = midFontSize,
  FontWeight textWeight = FontWeight.normal,
  EdgeInsets margin,
  EdgeInsets padding,
  Alignment alignment = Alignment.center,
  double borderRadius = 10,
}) {
  return Container(
    margin: margin ?? EdgeInsets.zero,
    padding: padding ?? EdgeInsets.zero,
    child: Text(
      text,
      maxLines: maxLines,
      overflow:overflow,
      style: TextStyle(
          color: textColor,

          height: textHeight,
          fontSize: textSize,
          fontWeight: textWeight),
    ),
  );
}
