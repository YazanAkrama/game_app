import 'package:flutter/cupertino.dart';
import 'package:game_app/constant/app_string.dart';

Widget smallSpace({double space = defaultSmallSpace}) {
  return SizedBox(
    height: space,
    width: space,
  );
}

Widget midSpace({double space = defaultMidSpace}) {
  return SizedBox(
    height: space,
    width: space,
  );
}

Widget bigSpace({double space = defaultBigSpace}) {
  return SizedBox(
    height: space,
    width: space,
  );
}
