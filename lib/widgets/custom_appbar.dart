import 'package:flutter/material.dart';
import 'package:game_app/constant/app_colors.dart';
import 'package:game_app/utils/navigator_back.dart';

Widget customAppbar(
  BuildContext context, {
  Widget title,
  bool centerTitle,
  bool withBack = false,
  Color appbarColor = mainAppColor,
  Widget actions,
}) {
  return AppBar(
    centerTitle: centerTitle,
    iconTheme: const IconThemeData(color: font_Color_1),
    backgroundColor: appbarColor,
    title: title ??Container(),
    actions: actions == null ? [] : [actions],
    leading: withBack
        ? null
        : Container(),
  );
}
