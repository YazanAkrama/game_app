import 'package:flutter/material.dart';
import 'package:game_app/constant/app_string.dart';
import '../constant/app_colors.dart';

Widget customBtn({
  String title = "...",
  Color titleColor = Colors.white,
  double titleHeight = 1,
  double titleSize = midFontSize,
  FontWeight titleWeight = FontWeight.normal,
  Function onTap,
  Color btnColor = mainAppColor,
  EdgeInsets margin,
  EdgeInsets padding,
  Alignment alignment = Alignment.center,
  double borderRadius = 10,
}) {
  return Container(
    margin: margin??EdgeInsets.zero,
    child: InkWell(
        onTap: onTap,
        child: Container(
          decoration: BoxDecoration(
            borderRadius: BorderRadius.circular(borderRadius),
            color: btnColor,
          ),
          alignment: alignment,
          padding: padding??EdgeInsets.zero,
          child: Text(
            title,
            style: TextStyle(
                color: titleColor,
                height: titleHeight,
                fontSize: titleSize,
                fontWeight: titleWeight),
          ),
        )),
  );
}
