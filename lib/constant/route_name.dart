


import 'package:flutter/material.dart';
import 'package:game_app/pages/add_page/view/add_game.dart';
import 'package:game_app/pages/auth/view/log_in.dart';
import 'package:game_app/pages/auth/view/sign_up.dart';
import 'package:game_app/pages/home_page/view/home_page.dart';

Map<String, Widget Function(BuildContext)> routes = {
  '/home_page': (context) => const HomePage(),
  '/add_page': (context) =>  AddGame(),
  '/log_in': (context) => const LogIn(),
  '/sign_up': (context) => const SignUp(),
};