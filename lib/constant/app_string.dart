

const double smallFontSize=12;
const double midFontSize=16;
const double bigFontSize=23;
const double defaultSmallSpace=10;
const double defaultMidSpace=20;
const double defaultBigSpace=30;