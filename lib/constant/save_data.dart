import 'package:shared_preferences/shared_preferences.dart';

Future<bool> setLocal(String local) async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  preferences.setString('Local', local);
  return preferences.commit();
}

Future<String> getLocal() async {
  SharedPreferences preferences = await SharedPreferences.getInstance();
  String local = preferences.get('Local');
  if (local != null) {
    return local;
  } else {
    return "ar";
  }
}
