// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

abstract class  LocaleKeys {
  static const log_in = 'log_in';
  static const create_account = 'create_account';
  static const dont_have_account = 'dont_have_account';
  static const create = 'create';
  static const home_page = 'home_page';
  static const add_game = 'add_game';
  static const full_name = 'full_name';
  static const username = 'username';
  static const password = 'password';
  static const add_page = 'add_page';
  static const title = 'title';
  static const description = 'description';
  static const geo = 'geo';
  static const player_count = 'player_count';
  static const date = 'date';
  static const players = 'players';
  static const confirm_delete = 'confirm_delete';
  static const accept = 'accept';
  static const title_in_arabic = 'title_in_arabic';
  static const description_in_arabic = 'description_in_arabic';
  static const description_in_english = 'description_in_english';
  static const title_in_english = 'title_in_english';
  static const arabic = 'arabic';
  static const english = 'english';
  static const data_error = 'data_error';
  static const not_complete_error = 'not_complete_error';
  static const selecet_date_time = 'selecet_date_time';
  static const account_success = 'account_success';
  static const log_out = 'log_out';
  static const cancel = 'cancel';
  static const save = 'save';
  static const phone_nunber = 'phone_nunber';
  static const insert_number = 'insert_number';
  static const sent = 'sent';
  static const check_phone_number = 'check_phone_number';
  static const username_exist = 'username_exist';
  static const empty_list = 'empty_list';
  static const geo_in_english = 'geo_in_english';
  static const geo_in_arabic = 'geo_in_arabic';

}
