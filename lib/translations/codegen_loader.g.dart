// DO NOT EDIT. This is code generated via package:easy_localization/generate.dart

// ignore_for_file: prefer_single_quotes

import 'dart:ui';

import 'package:easy_localization/easy_localization.dart' show AssetLoader;

class CodegenLoader extends AssetLoader{
  const CodegenLoader();

  @override
  Future<Map<String, dynamic>> load(String fullPath, Locale locale ) {
    return Future.value(mapLocales[locale.toString()]);
  }

  static const Map<String,dynamic> en = {
  "log_in": "Log in",
  "create_account": "Create account",
  "dont_have_account": "Don't have an account yet? create one",
  "create": "Create",
  "home_page": "Home page",
  "add_game": "Add game",
  "full_name": "Full name",
  "username": "Username",
  "password": "Password",
  "add_page": "Add page",
  "title": "title",
  "description": "Description",
  "geo": "location",
  "player_count": "Max players",
  "date": "Date",
  "players": "players",
  "confirm_delete": "Are you sure you want to delete this item ?",
  "accept": "Accept",
  "title_in_arabic": "Title in Arabic",
  "description_in_arabic": "Description in Arabic",
  "description_in_english": "Description in English",
  "title_in_english": "Title in English",
  "arabic": "Arabic",
  "english": "English",
  "data_error": "Data error",
  "not_complete_error": "Data not complete !",
  "selecet_date_time": "Set date and time",
  "account_success": "Account successfuly created",
  "log_out": "Log out",
  "cancel": "Cancel",
  "save": "Save",
  "phone_nunber": "Phone number",
  "insert_number": "insert whatsapp number with your country code, without + to send the detals",
  "sent": "Sent",
  "check_phone_number": "Check phone number",
  "username_exist": "Username exist",
  "empty_list": "No games yet",
  "geo_in_english": "Location in English",
  "geo_in_arabic": "Location in Arabic"
};
static const Map<String,dynamic> ar = {
  "log_in": "تسجيل الدخول",
  "create_account": "إنشاء حساب",
  "dont_have_account": "ليس لديك حساب؟ إنشئ حساب مجاناً",
  "create": "إنشاء",
  "home_page": "الصفحة الرئيسية",
  "add_game": "إضافة لعبة",
  "full_name": "الاسم الكامل",
  "username": "اسم المستخدم",
  "password": "كلمة السر",
  "add_page": "صفحة الاضافة",
  "title": "العنوان",
  "description": "الوصف",
  "geo": "الموقع الجغرافي",
  "player_count": "عدد اللاعبين الكلي",
  "date": "التاريخ",
  "players": "اللاعبين",
  "confirm_delete": "هل انت متآكد من حذف هذا العنصر ؟",
  "accept": "موافق",
  "title_in_arabic": "العنوان بالعربي",
  "description_in_arabic": "الوصف بالعربية",
  "description_in_english": "الوصف بالانكليزية",
  "title_in_english": "العنوان بالانكليزية",
  "arabic": "العربية",
  "english": "الانكليزية",
  "data_error": "يوجد خطآ في البيانات",
  "not_complete_error": "! البيانات غير مكتملة",
  "selecet_date_time": "ضبط التاريخ و الوقت",
  "account_success": "تم انشاء الحساب بنجاح ",
  "log_out": "تسجيل الخروج",
  "cancel": "الغاء",
  "save": "حفظ",
  "phone_nunber": "رقم الهاتف",
  "insert_number": "ادخل رقم الواتساب مع رمز البلد بدون + لارسال التفاصيل",
  "sent": "ارسال",
  "check_phone_number": "تاكد من رقم الهاتف",
  "username_exist": "اسم المستخدم موجود",
  "empty_list": "لا يوجد العاب بعد",
  "geo_in_english": "الموقع بالانكليزية",
  "geo_in_arabic": "الموقع بالعربية"
};
static const Map<String, Map<String,dynamic>> mapLocales = {"en": en, "ar": ar};
}
