import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_colors.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/functions/date_formate.dart';
import 'package:game_app/models/game_model/game_model.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_back.dart';
import 'package:game_app/widgets/custom_btn.dart';
import 'package:game_app/widgets/custom_space.dart';
import 'package:game_app/widgets/custom_text.dart';
import 'package:game_app/widgets/custom_text_field.dart';
import 'package:game_app/widgets/custom_toast.dart';
import 'package:url_launcher/url_launcher.dart';

class HomePageController extends ChangeNotifier {
  bool _hasNextPage = true;
  bool _isFirstLoadRunning = false;
  bool _isLoadMoreRunning = false;
  GameModel selectedGame;
  ScrollController scrollController;
  int _current_item = 0;
  int _limit_item = DataOfApp.gameList.length;

  HomePageController() {
    // setLimit_item(DataOfApp.userList.length);
    firstLoad();
    scrollController = ScrollController()..addListener(loadMore);
  }

  bool get hasNextPage => _hasNextPage;

  setHasNextPage(bool value) {
    _hasNextPage = value;
  }

  bool get isFirstLoadRunning => _isFirstLoadRunning;

  setIsFirstLoadRunning(bool value) {
    _isFirstLoadRunning = value;
    notifyListeners();
  }

  bool get isLoadMoreRunning => _isLoadMoreRunning;

  setIsLoadMoreRunning(bool value) {
    _isLoadMoreRunning = value;
    notifyListeners();
  }

  int get current_item => _current_item;

  setCurrent_item(int value) {
    _current_item = value;
  }

  int get limit_item => _limit_item;

  setLimit_item(int value) {
    _limit_item = value;
  }

  void firstLoad() async {
    setIsFirstLoadRunning(true);

    // I put a timer to simulate  http get request

    Future.delayed(const Duration(milliseconds: 2000), () {
      if (limit_item <= 10) {
        setCurrent_item(limit_item);
        setHasNextPage(false);
      } else {
        setCurrent_item(10);
      }
      setIsFirstLoadRunning(false);
    });
  }

  void updateData() async {
    if (current_item <= limit_item) {
      setLimit_item(DataOfApp.gameList.length);
      if (limit_item < 10) {
        setCurrent_item(limit_item);
        setHasNextPage(false);
      } else {
        setCurrent_item(10);
        setHasNextPage(true);
      }
    } else {
      setLimit_item(DataOfApp.gameList.length);
    }
  }

  void loadMore() async {
    if (hasNextPage &&
        !isFirstLoadRunning &&
        !isLoadMoreRunning &&
        scrollController.position.extentAfter < 10) {
      setIsLoadMoreRunning(true);

      // I put a timer to simulate  http get request

      Future.delayed(const Duration(milliseconds: 2000), () {
        if (limit_item <= (current_item + 10)) {
          setCurrent_item(limit_item);
          setHasNextPage(false);
        } else {
          setCurrent_item(current_item + 10);
        }
        setIsLoadMoreRunning(false);
      });
    }
  }

  inviteFriend(BuildContext context, GameModel game) {
    TextEditingController phone_number = TextEditingController();
    showDialog(
        context: context,
        builder: (context) => AlertDialog(
              title: customText(LocaleKeys.insert_number.tr(), maxLines: 2),
              content: SingleChildScrollView(
                  child: customTextField(
                      controller: phone_number,
                      inputType: TextInputType.number,
                      hintText: LocaleKeys.phone_nunber.tr(),
                      prefixIconData: Icons.phone)),
              actions: [
                customBtn(
                    padding: const EdgeInsets.only(top: 15, bottom: 15),
                    title: LocaleKeys.sent.tr(),
                    onTap: () {
                      if (phone_number.text != null &&
                          phone_number.text.isNotEmpty) {
                        sendInvite(game, context, phone_number.text);
                      } else {
                        customToast(LocaleKeys.data_error);
                      }
                    })
              ],
            ));
  }

  confirmDelete(BuildContext context, GameModel game) {
    showDialog(
        context: context,
        builder: (_) => AlertDialog(
              insetPadding: EdgeInsets.zero,
              content: customText(
                LocaleKeys.confirm_delete.tr(),
              ),
              actions: [
                Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly,children: [

                  customBtn(
                      btnColor: const Color(0xffdddddd),
                      titleColor: font_Color_2,
                      padding: const EdgeInsets.all(15),
                      title: LocaleKeys.cancel.tr(),
                      onTap: () {
                        navigatorBack(context);
                      }),
                  smallSpace(),

                  customBtn(
                      padding: const EdgeInsets.all(15),
                      btnColor: Colors.red,
                      title: LocaleKeys.accept.tr(),
                      onTap: () {
                        deleteGame(context, game);
                      }),],)

              ],
            ));
  }

  deleteGame(BuildContext context, GameModel game) async {
    var ex = DataOfApp.gameList.where((element) => element.id == game.id);
    if (ex.isNotEmpty) {
      DataOfApp.gameList.remove(ex.first);
      setLimit_item(limit_item - 1);
      setCurrent_item(current_item - 1);
      navigatorBack(context);
      notifyListeners();
    }
  }

  sendInvite(GameModel game, BuildContext context, String phone_number) async {
    String message =
        "${LocaleKeys.title.tr()}: ${game.title != null ? game.title["${context.locale}"] : ""}\n${LocaleKeys.date.tr()}: ${dateFormat(context, dateTime: game.date)}\n${LocaleKeys.player_count.tr()}: ${game.player_count}\n${LocaleKeys.description.tr()}: ${game.description != null ? game.description["${context.locale}"] : ""}";
    if (await canLaunch("https://wa.me/$phone_number?text=$message")) {
      await launch("https://wa.me/$phone_number?text=$message");
    } else {
      customToast(LocaleKeys.check_phone_number.tr());
    }
  }

  update() {
    updateData();
    notifyListeners();
  }
}
