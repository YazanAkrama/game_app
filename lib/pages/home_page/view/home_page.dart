import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_colors.dart';
import 'package:game_app/constant/app_string.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/pages/home_page/view/game_card_shape.dart';
import 'package:game_app/pages/home_page/viewModel/home_page_controller.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_to.dart';
import 'package:game_app/widgets/custom_appbar.dart';
import 'package:game_app/widgets/custom_drawer.dart';
import 'package:game_app/widgets/custom_text.dart';
import 'package:provider/provider.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    HomePageController controller = context.watch<HomePageController>();
    return Scaffold(
        drawer: customDrawer(context),
        appBar: customAppbar(context,
            withBack: true,
            title:
                customText(LocaleKeys.home_page.tr(), textColor: font_Color_1),
            centerTitle: true,
            actions: IconButton(
                onPressed: () {
                  navigatorTo(context, page_name: "/add_page");
                },
                icon: const Icon(Icons.add))),
        body: body(context, controller));
  }

  Widget body(BuildContext context, HomePageController controller) {
    if (controller.isFirstLoadRunning) {
      return const Center(
        child: CircularProgressIndicator(),
      );
    }
    return Column(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: [
        Expanded(
          child: controller.current_item == 0
              ? Align(
                alignment: Alignment.center,
                child: customText(LocaleKeys.empty_list.tr(),
                    textColor: const Color(0xff999999),
                    textSize: bigFontSize, textWeight: FontWeight.bold),
              )
              : ListView.builder(
                  controller: controller.scrollController,
                  itemCount: controller.current_item,
                  itemBuilder: (context, i) {
                    if (controller.isLoadMoreRunning &&
                        i == controller.current_item - 1) {
                      return Column(
                        children: [
                          gameCardShape(
                            context,
                            DataOfApp.gameList[i],
                            controller,
                          ),
                          const Padding(
                            padding: EdgeInsets.only(top: 10, bottom: 40),
                            child: Center(
                              child: CircularProgressIndicator(),
                            ),
                          ),
                        ],
                      );
                    } else {
                      return gameCardShape(
                        context,
                        DataOfApp.gameList[i],
                        controller,
                      );
                    }
                  }),
        ),
      ],
    );
  }
}
