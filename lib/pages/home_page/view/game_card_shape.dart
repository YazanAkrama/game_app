import 'dart:io';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_string.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/functions/date_formate.dart';
import 'package:game_app/models/game_model/game_model.dart';
import 'package:game_app/pages/home_page/viewModel/home_page_controller.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_to.dart';
import 'package:game_app/widgets/custom_text.dart';

Widget gameCardShape(
    BuildContext context, GameModel game, HomePageController controller) {
  return Row(
    children: [
      Column(
        children: [
          IconButton(
              onPressed: () {
                controller.inviteFriend(context, game);
              },
              icon: const Icon(Icons.share)),
          DataOfApp.myUser.id == game.user_id
              ? IconButton(
                  onPressed: () {
                    navigatorTo(context,
                        page_name: "/add_page", arguments: game);
                  },
                  icon: const Icon(Icons.edit))
              : Container(),
          DataOfApp.myUser.id == game.user_id
              ? IconButton(
                  onPressed: () {
                    controller.confirmDelete(context, game);
                  },
                  icon: const Icon(Icons.delete))
              : Container(),
        ],
      ),
      Expanded(
        child: Container(
          decoration: BoxDecoration(boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
              blurRadius: 2,
              offset: const Offset(0, 2), // changes position of shadow
            ),
          ], color: Colors.white, borderRadius: BorderRadius.circular(10)),
          margin: const EdgeInsets.all(10),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                height: 120,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(10),
                    image: DecorationImage(
                        image: game.image != null && game.image.isNotEmpty
                            ? FileImage(File(game.image))
                            : const AssetImage("assets/images/thumbnail.jpg"),
                        fit: BoxFit.cover)),
              ),
              Padding(
                padding: const EdgeInsets.all(5.0),
                child: Row(
                  children: [
                    Expanded(
                      child: Align(
                          alignment: AlignmentDirectional.centerStart,
                          child: customText(
                              game.title != null
                                  ? game.title[context.locale.toString()] ??
                                      ""
                                  : "",
                              maxLines: 1,
                              textSize: bigFontSize,
                              textWeight: FontWeight.bold)),
                    ),
                    customText(
                      dateFormat(context,
                          dateTime: game.date ?? DateTime.now(),
                          withTime: true),
                      maxLines: 1,
                    ),
                  ],
                ),
              ),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: customText(
                      "${game.player_count ?? ""} ${LocaleKeys.players.tr()}",
                      maxLines: 2,
                      textSize: midFontSize)),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: customText(
                      game.geo_location != null
                          ? game.geo_location[context.locale.toString()] ?? ""
                          : "",
                      maxLines: 2,
                      textSize: midFontSize)),
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: customText(
                      game.description != null
                          ? game.description[context.locale.toString()] ?? ""
                          : "",
                      maxLines: null,
                      textColor: const Color(0xff999999),
                      textSize: midFontSize))
            ],
          ),
        ),
      ),
    ],
  );
}
