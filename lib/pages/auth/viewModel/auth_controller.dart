import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/functions/user_exist.dart';
import 'package:game_app/models/user_model/user_model.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_back.dart';
import 'package:game_app/utils/navigator_off_to.dart';
import 'package:game_app/widgets/custom_toast.dart';
import 'package:game_app/widgets/loading.dart';
import 'package:uuid/uuid.dart';

class AuthController extends ChangeNotifier {
  TextEditingController usernameController = TextEditingController();
  TextEditingController fullNameController = TextEditingController();
  TextEditingController passwordController = TextEditingController();

  createValidator() {
    if (fullNameController.text == null || fullNameController.text.isEmpty) {
      return false;
    }
    if (usernameController.text == null || usernameController.text.isEmpty) {
      return false;
    }
    if (passwordController.text == null || passwordController.text.isEmpty) {
      return false;
    }
    return true;
  }

  loginValidator() {
    if (usernameController.text == null || usernameController.text.isEmpty) {
      return false;
    }
    if (passwordController.text == null || passwordController.text.isEmpty) {
      return false;
    }
    var ex = DataOfApp.userList.where((element) =>
        element.username == usernameController.text &&
        element.password == passwordController.text);
    if (ex.isNotEmpty) {
      DataOfApp.myUser = ex.first;
      return true;
    } else {
      return false;
    }
  }

  createAccount(BuildContext context) {
    loading(context);
    if (createValidator()) {
      UserModel user = UserModel(const Uuid().v1(), usernameController.text,
          passwordController.text, fullNameController.text);
      if(userExist(user)){
        navigatorBack(context);
        customToast(LocaleKeys.username_exist.tr());
      }else{
        DataOfApp.userList.add(user);
        customToast(LocaleKeys.account_success.tr());
        navigatorBack(context);
        disposeData();
        navigatorToOff(context, page_name: "/log_in");
      }
    } else {
      navigatorBack(context);
      customToast(LocaleKeys.not_complete_error.tr());
    }
  }

  disposeData() {
    passwordController.clear();
    fullNameController.clear();
    usernameController.clear();
  }

  login(BuildContext context) {
    loading(context);
    if (loginValidator()) {
      navigatorBack(context);
      disposeData();
      navigatorToOff(context, page_name: "/home_page");
    } else {
      navigatorBack(context);
      customToast(LocaleKeys.data_error.tr());
    }
  }
}
