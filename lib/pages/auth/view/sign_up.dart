import 'package:flutter/material.dart';
import 'package:game_app/constant/app_string.dart';
import 'package:game_app/pages/auth/viewModel/auth_controller.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/widgets/custom_btn.dart';
import 'package:game_app/widgets/custom_space.dart';
import 'package:game_app/widgets/custom_text.dart';
import 'package:game_app/widgets/custom_text_field.dart';
import 'package:provider/provider.dart';
import 'package:easy_localization/easy_localization.dart';
class SignUp extends StatelessWidget {
  const SignUp({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthController controller = context.watch<AuthController>();
    return Scaffold(
      body: Container(
        margin: const EdgeInsets.only(right: 20, left: 20),
        child: Center(
            child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              customText(LocaleKeys.create_account.tr(),
                  textSize: bigFontSize, textWeight: FontWeight.bold),
              bigSpace(),
              customTextField(
                controller: controller.fullNameController,
                hintText: LocaleKeys.full_name.tr(),
                prefixIconData: Icons.person,
              ),
              midSpace(),
              customTextField(
                controller: controller.usernameController,
                hintText: LocaleKeys.username.tr(),
                prefixIconData: Icons.person,
              ),
              midSpace(),
              customTextField(
                controller: controller.passwordController,
                hintText: LocaleKeys.password.tr(),
                prefixIconData: Icons.lock,
              ),
              bigSpace(),
              customBtn(
                title: LocaleKeys.create.tr(),
                onTap: () {
                  controller.createAccount(context);
                },
                padding: const EdgeInsets.only(top: 15, bottom: 15),
              ),
            ],
          ),
        )),
      ),
    );
  }
}
