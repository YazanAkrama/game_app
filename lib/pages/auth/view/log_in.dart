import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/app_string.dart';
import 'package:game_app/pages/auth/viewModel/auth_controller.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_to.dart';
import 'package:game_app/widgets/custom_btn.dart';
import 'package:game_app/widgets/custom_space.dart';
import 'package:game_app/widgets/custom_text.dart';
import 'package:game_app/widgets/custom_text_field.dart';
import 'package:provider/provider.dart';

class LogIn extends StatelessWidget {
  const LogIn({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AuthController controller = context.watch<AuthController>();
    return Scaffold(
        body: SafeArea(
      child: Container(
          margin: const EdgeInsets.only(right: 20, left: 20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.end,
            children: [
             context.locale.toString()=="en"? TextButton(
                  onPressed: () async {
                    await context.setLocale(
                      const Locale('ar'),
                    );
                  },
                  child: customText("العربية",
                      textSize: bigFontSize, textWeight: FontWeight.bold)):TextButton(
                 onPressed: () async {
                   await context.setLocale(
                     const Locale('en'),
                   );
                 },
                 child: customText("English",
                     textSize: bigFontSize, textWeight: FontWeight.bold)),
              Expanded(
                child: Center(
                    child: SingleChildScrollView(
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      customText(LocaleKeys.log_in.tr(),
                          textSize: bigFontSize, textWeight: FontWeight.bold),
                      bigSpace(),
                      customTextField(
                        controller: controller.usernameController,
                        hintText: LocaleKeys.username.tr(),
                        prefixIconData: Icons.person,
                      ),
                      midSpace(),
                      customTextField(
                        controller: controller.passwordController,
                        hintText: LocaleKeys.password.tr(),
                        prefixIconData: Icons.lock,
                      ),
                      midSpace(),
                      customBtn(
                        title: LocaleKeys.log_in.tr(),
                        onTap: () {
                          controller.login(context);
                        },
                        padding: const EdgeInsets.only(top: 15, bottom: 15),
                      ),
                      midSpace(),
                      customText(LocaleKeys.dont_have_account.tr()),
                      midSpace(),
                      customBtn(
                        title: LocaleKeys.create_account.tr(),
                        onTap: () {
                          controller.disposeData();
                          navigatorTo(context, page_name: "/sign_up");
                        },
                        padding: const EdgeInsets.only(top: 15, bottom: 15),
                      ),
                    ],
                  ),
                )),
              ),
            ],
          )),
    ));
  }
}
