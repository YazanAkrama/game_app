import 'dart:io';

import 'package:easy_localization/src/public_ext.dart';
import 'package:flutter/material.dart';
import 'package:game_app/constant/data.dart';
import 'package:game_app/models/game_model/game_model.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_back.dart';
import 'package:game_app/widgets/custom_toast.dart';
import 'package:image_picker/image_picker.dart';
import 'package:uuid/uuid.dart';

class AddController extends ChangeNotifier {
  String _title_ar, _title_en, _description_ar, _description_en, _player_count,_geo_location_en,_geo_location_ar;
  DateTime _date;
  File _imageSelected;

  File get imageSelected => _imageSelected;

  setImageSelected(File value) {
    _imageSelected = value;
    notifyListeners();
  }

  get geo_location_en => _geo_location_en;

  setGeo_location_en(value) {
    _geo_location_en = value;
  }

  get geo_location_ar => _geo_location_ar;

  setGeo_location_ar(value) {
    _geo_location_ar = value;
  }


  String get title_ar => _title_ar;

  setTitle_ar(String value) {
    _title_ar = value;
  }

  get title_en => _title_en;

  setTitle_en(value) {
    _title_en = value;
  }

  get description_ar => _description_ar;

  setDescription_ar(value) {
    _description_ar = value;
  }

  get description_en => _description_en;

  setDescription_en(value) {
    _description_en = value;
  }

  String get player_count => _player_count;

  setPlayer_count(String value) {
    _player_count = value;
  }

  DateTime get date => _date;

  setDate(DateTime value) {
    _date = value;
    notifyListeners();
  }

  Future getImg(int m) async {
    XFile pickedFile = await ImagePicker().pickImage(
        imageQuality: 70,
        source: m == 1 ? ImageSource.camera : ImageSource.gallery,
        maxHeight: 800,
        maxWidth: 800);
    if (pickedFile != null) {
      setImageSelected(File(pickedFile.path));
    } else {}
  }

  choseImagePath(BuildContext context) {
    Widget cameraButton = IconButton(
        iconSize: 75,
        icon: Icon(
          Icons.camera_alt,
          color: Colors.grey.withOpacity(.6),
        ),
        onPressed: () async {
          getImg(1);
          navigatorBack(context);
        });

    Widget galleryButton = IconButton(
        iconSize: 75,
        icon: Icon(
          Icons.image,
          color: Colors.grey.withOpacity(.6),
        ),
        onPressed: () async {
          getImg(2);
          navigatorBack(context);
        });
    Widget myDialog = AlertDialog(
      title: Container(
        decoration: BoxDecoration(borderRadius: BorderRadius.circular(30)),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [galleryButton, cameraButton],
        ),
      ),
    );
    showDialog(context: context, builder: (context) => myDialog);
  }

  Future<DateTime> pickDate(BuildContext context) => showDatePicker(
      context: context,
      initialDate: date ?? DateTime.now(),
      firstDate: DateTime(2000),
      lastDate: DateTime(2030));

  Future<TimeOfDay> pickTime(BuildContext context) => showTimePicker(
      context: context,
      initialTime: TimeOfDay(
          hour: date == null ? DateTime.now().hour : date.hour,
          minute: date == null ? DateTime.now().minute : date.minute));

  bool validator() {
    if (title_ar == null || title_en == null) return false;
    if (player_count == null) return false;

    return true;
  }

  saveGame(BuildContext context, GameModel selectedGame) {
    if (selectedGame == null) {
      createNewGame(context);
    } else {
      editGame(context, selectedGame);
    }
  }

  createNewGame(BuildContext context) {
    if (validator()) {
      GameModel gameModel = GameModel(
          const Uuid().v1(),
          {"ar": title_ar, "en": title_en},
          int.parse(player_count),
          date != null ? date.toString() : DateTime.now().toString(),
          DataOfApp.myUser.id,
          created_at: DateTime.now().toString(),
          description: {"ar": description_ar, "en": description_en},
          geo_location:  {"ar": geo_location_ar, "en": geo_location_en},
          image: imageSelected != null ? imageSelected.path : "");
      DataOfApp.gameList.add(gameModel);
      disposeData();
      navigatorBack(context);
      notifyListeners();
    } else {
      customToast(LocaleKeys.not_complete_error.tr());
    }
  }

  disposeData() {
    setPlayer_count(null);
    setDate(null);
    setDescription_ar(null);
    setDescription_en(null);
    setTitle_ar(null);
    setTitle_en(null);
    setGeo_location_en(null);
    setGeo_location_ar(null);
    setImageSelected(null);
  }

  editGame(BuildContext context, GameModel selectedGame) {
    GameModel gameModel = GameModel(
        selectedGame.id,
        {
          "ar": title_ar ?? selectedGame.title["ar"],
          "en": title_en ?? selectedGame.title["en"]
        },
        player_count != null
            ? int.parse(player_count)
            : selectedGame.player_count,

        date != null ? date.toString() : selectedGame.date.toString(),
        DataOfApp.myUser.id,
        created_at: DateTime.now().toString(),
        description: {
          "ar": description_ar ?? selectedGame.description["ar"],
          "en": description_en ?? selectedGame.description["en"]
        },
        geo_location: {
          "ar": geo_location_ar ?? selectedGame.geo_location["ar"],
          "en": geo_location_en ?? selectedGame.geo_location["en"]
        },
        image: imageSelected != null ? imageSelected.path : selectedGame.image);

    DataOfApp.gameList[DataOfApp.gameList
            .indexWhere((element) => element.id == selectedGame.id)] =
        gameModel;
    disposeData();
    navigatorBack(context);
    notifyListeners();
  }

}
