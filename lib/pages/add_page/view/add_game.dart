import 'dart:io';

import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:game_app/constant/app_colors.dart';
import 'package:game_app/functions/check_permission.dart';
import 'package:game_app/functions/date_formate.dart';
import 'package:game_app/models/game_model/game_model.dart';
import 'package:game_app/pages/add_page/viewModel/add_controller.dart';
import 'package:game_app/translations/locale_keys.g.dart';
import 'package:game_app/utils/navigator_back.dart';
import 'package:game_app/widgets/custom_appbar.dart';
import 'package:game_app/widgets/custom_btn.dart';
import 'package:game_app/widgets/custom_space.dart';
import 'package:game_app/widgets/custom_text.dart';
import 'package:game_app/widgets/custom_text_field.dart';
import 'package:provider/provider.dart';

class AddGame extends StatelessWidget {
  GameModel selectedGame;

  AddGame({
    Key key,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    AddController controller = context.watch<AddController>();
    selectedGame = ModalRoute.of(context).settings.arguments;
    return Scaffold(
        appBar: customAppbar(context,
            title: customText(LocaleKeys.add_game.tr(),textColor: font_Color_1),
            centerTitle: true,
            actions: IconButton(
                onPressed: () {
                  controller.disposeData();
                  navigatorBack(context);
                },
                icon: const Icon(Icons.arrow_forward))),
        body: WillPopScope(
            onWillPop: () async {
              controller.disposeData();
              return true;
            },
            child: body(context, controller)));
  }

  Widget body(BuildContext context, AddController controller) {
    return DefaultTabController(
      length: 2,
      child: Column(
        children: [
          TabBar(
              unselectedLabelColor: mainAppColor.withOpacity(.5),
              indicatorColor: mainAppColor,
              labelColor: mainAppColor,
              tabs: [
                Tab(
                  text: LocaleKeys.arabic.tr(),
                ),
                Tab(text: LocaleKeys.english.tr()),
              ]),
          Expanded(
            child: SingleChildScrollView(
              child: Column(
                children: [
                  InkWell(
                    onTap: () async {
                      if (await checkPermissionForCameraAndGallery()) {
                        controller.choseImagePath(context);
                      }
                    },
                    child: Container(
                      margin:
                          const EdgeInsets.only(top: 15, right: 10, left: 10),
                      height: 160,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(15),
                            image: DecorationImage(
                                image: controller.imageSelected != null
                                    ? FileImage(
                                        File(controller.imageSelected.path))
                                    : selectedGame == null ||
                                            selectedGame.image == null ||
                                            selectedGame.image.isEmpty
                                        ? const AssetImage(
                                            "assets/images/thumbnail.jpg",
                                          )
                                        : FileImage(File(selectedGame.image)),
                                fit: BoxFit.cover)),
                      ),
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10, right: 10, left: 10, ),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        customText(LocaleKeys.selecet_date_time.tr()),
                        smallSpace(),
                        customBtn(
                          title: dateFormat(context,
                              dateTime: controller.date == null
                                  ? selectedGame != null &&
                                          selectedGame.date != null
                                      ? selectedGame.date
                                      : DateTime.now().toString()
                                  : controller.date.toString(),
                              withTime: true),
                          onTap: () async {
                            final date = await controller.pickDate(context);
                            if (date == null) return;
                            final time = await controller.pickTime(context);
                            if (time == null) return;
                            controller.setDate(DateTime(date.year, date.month,
                                date.day, time.hour, time.minute));
                          },
                          padding: const EdgeInsets.only(top: 15, bottom: 15),
                        ),
                        smallSpace(),
                        customTextField(
                          initialValue: selectedGame != null
                              ? selectedGame.player_count.toString() ??
                              controller.player_count
                              : controller.player_count,
                          textInputFormatter: [FilteringTextInputFormatter.allow(RegExp('[0-9]'))],
                          inputType: TextInputType.number,
                          hintText: LocaleKeys.player_count.tr(),
                          onChange: (value) {
                            controller.setPlayer_count(value);
                          },
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 270,
                    margin: const EdgeInsets.only(top: 10, right: 10, left: 10),
                    child: TabBarView(
                      children: [
                        arabicInput(controller),
                        englishInput(controller),
                      ],
                    ),
                  ),
                  Container(
                    margin: const EdgeInsets.only(top: 10, right: 10, left: 10,bottom: 10),
                    child: customBtn(
                      title: LocaleKeys.save.tr(),
                      onTap: () {
                        controller.saveGame(context, selectedGame);
                      },
                      padding: const EdgeInsets.only(top: 15, bottom: 15),
                    ),
                  )
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  arabicInput(AddController controller) {
    return Column(
      children: [
        customTextField(
          initialValue: selectedGame != null
              ? selectedGame.title["ar"] ?? controller.title_ar
              : controller.title_ar,
          hintText: LocaleKeys.title_in_arabic.tr(),
          onChange: (value) {
            controller.setTitle_ar(value);
          },
        ),
        smallSpace(),
        customTextField(
          inputType: TextInputType.multiline,
          maxLines: 3,
          initialValue: selectedGame != null
              ? selectedGame.description["ar"] ?? controller.description_ar
              : controller.description_ar,
          hintText: LocaleKeys.description_in_arabic.tr(),
          onChange: (value) {
            controller.setDescription_ar(value);
          },
        ),
        smallSpace(),
        customTextField(
          inputType: TextInputType.multiline,
          maxLines: 3,
          initialValue: selectedGame != null
              ? selectedGame.geo_location["ar"] ?? controller.geo_location_ar
              : controller.geo_location_ar,
          hintText: LocaleKeys.geo_in_arabic.tr(),
          onChange: (value) {
            controller.setGeo_location_ar(value);
          },
        ),
      ],
    );
  }

  englishInput(AddController controller) {
    return Column(
      children: [
        customTextField(
          initialValue: selectedGame != null
              ? selectedGame.title["en"] ?? controller.title_en
              : controller.title_en,
          hintText: LocaleKeys.title_in_english.tr(),
          onChange: (value) {
            controller.setTitle_en(value);
          },
        ),
        smallSpace(),
        customTextField(
          inputType: TextInputType.multiline,
          maxLines: 3,
          initialValue: selectedGame != null
              ? selectedGame.description["en"] ?? controller.description_en
              : controller.description_en,
          hintText: LocaleKeys.description_in_english.tr(),
          onChange: (value) {
            controller.setDescription_en(value);
          },
        ),
        smallSpace(),
        customTextField(
          inputType: TextInputType.multiline,
          maxLines: 3,
          initialValue: selectedGame != null
              ? selectedGame.geo_location["en"] ?? controller.geo_location_en
              : controller.geo_location_en,
          hintText: LocaleKeys.geo_in_english.tr(),
          onChange: (value) {
            controller.setGeo_location_en(value);
          },
        ),
      ],
    );
  }
}
